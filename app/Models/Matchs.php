<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Matchs extends Model
{
    use HasFactory;

    protected $table = 'matchs';
    protected $fillable = [
        'team_away',
        'team_home',
        'score_home',
        'score_away',
        'win',
        'lose',
        'draw',
        'GM',
        'GK',
        'total_point',
        'date'
    ];
}
