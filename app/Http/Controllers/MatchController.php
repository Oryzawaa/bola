<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Matchs;

class MatchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $match = Matchs::All();
        return view('match.index' , compact('match'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'team_home' => 'required',
            'team_away' => 'required',
            'score_home' => 'required',
            'score_away' => 'required',
            'date' => 'required',

        ]);

        $match = Matchs::all();

        if($request->team_home == $request->team_away){
            return redirect()->route('match.index')
            ->with('error' , 'Cannot input same');
        }

        if($request->score_home > $request->score_away){
            Matchs::create([
                'team_home' => $request->team_home,
                'team_away' => $request->team_away,
                'score_home' => $request->score_home,
                'score_away' => $request->score_away,
                'win' => 'win',
                'date' => $request->date,
                'status' => $request->status
            ]);
        }

        {
            Matchs::create([
                'team_home' => $request->team_home,
                'team_away' => $request->team_away,
                'date' => $request->date,
                'status' => $request->status
            ]);

            // dd($request);

            return redirect()->route('match.index')
            ->with('success' , 'Data berhasil disimpan');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($match)
    {
        $match = Matchs::all();

        return view('match.edit' , compact('match'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
