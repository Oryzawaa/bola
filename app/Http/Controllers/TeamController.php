<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Team};

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $team = Team::all();

        return view('team.index' , compact('team'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name_team' => 'required',
            'city' => 'required'
        ]);

        $team = Team::where('name_team' , $request->name_team)->first();

        if($team){
            return redirect()->route('team.index')
            ->with('error' , 'Team is Avaible');
        }else
        {
            Team::create([
                'name_team' => $request->name_team,
                'city' => $request->city,
                'point' => 0,
            ]);

            return redirect()->route('team.index')
            ->with('success' , 'Data berhasil disimpan');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Team::where('id' , $id)->delete();

        return redirect()->route('team.index')
        ->with('success' , 'Data berhasil dihapus');
    }
}
