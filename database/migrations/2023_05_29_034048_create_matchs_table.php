<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatchsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matchs', function (Blueprint $table) {
            $table->id();
            $table->string('team_home');
            $table->string('team_away');
            $table->integer('score_home');
            $table->integer('score_away');
            $table->integer('win');
            $table->integer('draw');
            $table->integer('lose');
            $table->integer('GM');
            $table->integer('GK');
            $table->integer('total_point');
            $table->date('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matchs');
    }
}
