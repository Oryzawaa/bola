<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\{HomeController , TeamController , MatchController};

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/index', function () {
    return view('index');
});

Route::get('Home' , [HomeController::class , 'index'])->name('home.index');

Route::get('Team' , [TeamController::class , 'index'])->name('team.index');
Route::post('Team/store' , [TeamController::class , 'store'])->name('team.store');
Route::delete('Team/delete/{id}' , [TeamController::class , 'destroy'])->name('team.delete');
Route::put('team/update/{id}' , [TeamController::class , 'update'])->name('team.update');

Route::get('Match' , [MatchController::class , 'index'])->name('match.index');
Route::post('Match/store' , [MatchController::class , 'store'])->name('match.store');
Route::get('Match/{id}' , [MatchController::class , 'edit'])->name('match.edit');


